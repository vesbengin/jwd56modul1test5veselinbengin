package model;


public class Proizvod {

	private int id;
	private String sifra;
	private String naziv;
	private double cena;
	private boolean besplatnaDostava;
	
//	private ArrayList<Porudzbina> porudzbina = new ArrayList<>();
	
	public Proizvod() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Proizvod(int id, String sifra, String naziv, double cena, boolean besplatnaDostava) {
		super();
		this.id = id;
		this.sifra = sifra;
		this.naziv = naziv;
		this.cena = cena;
		this.besplatnaDostava = besplatnaDostava;
	}

	@Override
	public String toString() {
		return "Proizvod [id=" + id + ", sifra=" + sifra + ", naziv=" + naziv + ", cena=" + cena + ", besplatnaDostava="
				+ besplatnaDostava + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Proizvod)) {
			return false;
		}
		Proizvod other = (Proizvod) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public boolean isBesplatnaDostava() {
		return besplatnaDostava;
	}

	public void setBesplatnaDostava(boolean besplatnaDostava) {
		this.besplatnaDostava = besplatnaDostava;
	}

//	public ArrayList<Porudzbina> getPorudzbina() {
//		return porudzbina;
//	}
//
//	public void setPorudzbina(ArrayList<Porudzbina> porudzbina) {
//		this.porudzbina = porudzbina;
//	}
	
	
	
	
}
