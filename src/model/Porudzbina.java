package model;

import java.time.LocalDateTime;

public class Porudzbina {
	
	private int id;
	private LocalDateTime datum;
	private String ulica;
	private int broj;
	private Proizvod proizvod;
	
	public Porudzbina() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Porudzbina(int id, LocalDateTime datum, String ulica, int broj, Proizvod proizvod) {
		super();
		this.id = id;
		this.datum = datum;
		this.ulica = ulica;
		this.broj = broj;
		this.proizvod = proizvod;
	}

	@Override
	public String toString() {
		return "Porudzbina [id=" + id + ", datum=" + datum + ", ulica=" + ulica + ", broj=" + broj + ", proizvod="
				+ proizvod + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Porudzbina other = (Porudzbina) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getDatum() {
		return datum;
	}

	public void setDatum(LocalDateTime datum) {
		this.datum = datum;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public int getBroj() {
		return broj;
	}

	public void setBroj(int broj) {
		this.broj = broj;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}
	
	
	
}
