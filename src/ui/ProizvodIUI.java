package ui;

import java.time.LocalDateTime;
import java.util.List;

import dao.ProizvodDAO;
import dnl.utils.text.table.TextTable;
import model.Proizvod;
import utils.PomocnaKlasaDatumi;

public class ProizvodIUI {
	
	public static void ispis() {
		
		try {
			List<Proizvod> proizvodi = ProizvodDAO.getAll();
			System.out.printf("%10s %10s %10s %10s %20s\n", 
					"Id",
					"Sifra",
					"Naziv",
					"Cena",
					"Besplatna dosava");
			for(Proizvod itP : proizvodi) {
				System.out.printf("%10d %10s %10s %10.2f %20s\n",
						itP.getId(),
						itP.getSifra(),
						itP.getNaziv(),
						itP.getCena(),
						itP.isBesplatnaDostava()? "DA" : "NE");
			}
			
			
		} catch (Exception e) {
			System.out.println("Greska sa bazom");
			e.printStackTrace();
		}
		
	}
	
	public static void ispisJTextUtils () {
		
			String[] columnNames = {
					"Id",
					"Sifra",
					"Naziv",
					"Cena",
					"Besplatna dosava"};
			
			try {
				List<Proizvod> proizvodi = ProizvodDAO.getAll();
				
				for(int i = 0; i < proizvodi.size(); i++) {
					proizvodi.get(i).getCena();
				}
						Object[][] data = {
						{proizvodi.get(0).getId(), proizvodi.get(0).getSifra(),
						proizvodi.get(0).getNaziv(), proizvodi.get(0).getCena(), proizvodi.get(0).isBesplatnaDostava()},
						{proizvodi.get(1).getId(), proizvodi.get(1).getSifra(),
						proizvodi.get(1).getNaziv(), proizvodi.get(1).getCena(), proizvodi.get(1).isBesplatnaDostava()},
						{proizvodi.get(2).getId(), proizvodi.get(2).getSifra(),
						proizvodi.get(2).getNaziv(), proizvodi.get(2).getCena(), proizvodi.get(2).isBesplatnaDostava()}
						};

						TextTable tt = new TextTable(columnNames, data);
						// this adds the numbering on the left
						tt.setAddRowNumbering(true);
						// sort by the first column
						tt.setSort(0);
						tt.printTable();
				
			} catch (Exception e) {
				System.out.println("Greska sa bazom");
				e.printStackTrace();
			}
	}
	
	
	public static void ispisIzvestaj() {
		System.out.println("Unesi datum od");
		LocalDateTime datumOd = PomocnaKlasaDatumi.ocitajDatumVreme();
		System.out.println("Unesi datum do");
		LocalDateTime datumDo = PomocnaKlasaDatumi.ocitajDatumVreme();
		
		try {
			List<String> izvesaj = ProizvodDAO.getAllIzvestaj(datumOd, datumDo);
			System.out.printf("%10s %10s %15s %30s\n", 
					"Sifra",
					"Naziv",
					"Ukupan prihod",
					"Poslednja prodaja ");
			for(int i = 0; i < izvesaj.size(); i++) {
				String [] line = izvesaj.get(i).split(",");
				LocalDateTime datum = LocalDateTime.parse(line[3]);
				System.out.printf("%10s %10s %15s %30s\n", 
					line[0],
					line[1],
					line[2],
					datum.format(PomocnaKlasaDatumi.DATE_TIME_FORMATTER));
			}			
			
		} catch (Exception e) {
			System.out.println("Greska sa bazom");
			e.printStackTrace();
		}
		
	}

}
