package ui;

import java.time.LocalDateTime;
import java.util.List;

import dao.PorudzbinaDAO;
import dao.ProizvodDAO;
import model.Porudzbina;
import model.Proizvod;
import utils.PomocnaKlasaDatumi;

public class PorudzbinaUI {
	
	public static void ispis() {
		
		try {
			List<Porudzbina> porudzbine = PorudzbinaDAO.getAll();
			System.out.printf("%10s %30s %35s %10s %20s\n",
					"Id",
					"Datum",
					"Ulica",
					"Broj",
					"Proizvod");
			System.out.println("--------------------------------------------------");
			for(Porudzbina itP : porudzbine) {
				System.out.printf("%10d %30s %35s %10d %20s\n",
						itP.getId(),
						itP.getDatum().format(PomocnaKlasaDatumi.DATE_TIME_FORMATTER),
						itP.getUlica(),
						itP.getBroj(),
						itP.getProizvod().getNaziv());
			}
			
		} catch (Exception e) {
			System.out.println("Greska sa bazom");
			e.printStackTrace();
		}
		
	}
	
	public static void unos() {
		try {
			System.out.println("Unesi sifru proizvod za porudzbinu");
			String sifra = PomocnaKlasaDatumi.ocitajTekst();
			Proizvod proizvod = ProizvodDAO.getProizvodBySifra(sifra);
			
			if(proizvod == null) {
				System.out.println("Ne postoji proizvod u bazi");
				return;
			}
			
			LocalDateTime datum = LocalDateTime.now();
			System.out.println("Unesite ulicu porudzbine");
			String ulica = PomocnaKlasaDatumi.ocitajTekst();
			System.out.println("Unesite broj porudzbine");
			int broj = PomocnaKlasaDatumi.ocitajCeoBroj();
//			char odluka = PomocnaKlasaDatumi.ocitajOdlukuOPotvrdi("besplatnu dostavu?");
//			boolean besplatnaDostava;
//			if(odluka == 'Y') {
//				besplatnaDostava = true;
//			}else if (odluka == 'N'){
//				besplatnaDostava = false;
//			}
			
			Porudzbina porudbina = new Porudzbina(0, datum, ulica, broj, proizvod);
			
			PorudzbinaDAO.add(porudbina);
			
		} catch (Exception e) {
			System.out.println("Greska sa bazom");
			e.printStackTrace();
		}
	}
	
	

}
