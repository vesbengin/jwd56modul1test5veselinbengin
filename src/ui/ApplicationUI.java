package ui;

import dao.ConnectionManager;
import utils.PomocnaKlasaDatumi;

public class ApplicationUI {
	
	private static void ispisiMenu() {
		System.out.println();
		System.out.println("Porudzbine - Meni:");
		System.out.println("\t1 - Prikaz svih proizvoda");
		System.out.println("\t2 - Prikaz svih porudžbina sa pridruženim proizvodom");
		System.out.println("\t3 - Dodavanje porudžbine");
		System.out.println("\t4 - j-text-util proizvoda");
		System.out.println("\t5 - Izvestaj");
		System.out.println("\tx - IZLAZ IZ PROGRAMA");
	}

	public static void main(String[] args) {
		try {
			ConnectionManager.open();
		} catch (Exception ex) {
			System.out.println("Neuspesna konekcija na bazu!");

			ex.printStackTrace();
			return;
		}

		String odluka = "";
		while (!odluka.equals("x")) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = PomocnaKlasaDatumi.ocitajTekst();
			switch (odluka) {				
				case "1":
					ProizvodIUI.ispis();
					break;
				case "2":
					PorudzbinaUI.ispis();
					break;
				case "3":
					PorudzbinaUI.unos();
					break;
				case "4":
					ProizvodIUI.ispisJTextUtils();
					break;
				case "5":
					ProizvodIUI.ispisIzvestaj();
					break;
				case "x":
					System.out.println("Izlaz");
					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;
			}
		}

		try {
			ConnectionManager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
