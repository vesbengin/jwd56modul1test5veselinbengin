package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import model.Proizvod;

public class ProizvodDAO {
	
	public static Proizvod getProizvodByID(int id) throws Exception {
		Proizvod proizvod = null;

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String sql = "SELECT sifra, naziv, cena, besplatna_dostava FROM proizvod WHERE id = " + id;

			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			if (rset.next()) {
				int index = 1;
				String sifra = rset.getString(index++);
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				boolean besplatnaD = rset.getBoolean(index++);
				
				proizvod = new Proizvod(id, sifra, naziv, cena, besplatnaD);
			}
		} finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
			try {rset.close();} catch (Exception ex) {ex.printStackTrace();}
		}

		return proizvod;
	}
	
	public static Proizvod getProizvodBySifra(String sifra) throws Exception {
		Proizvod proizvod = null;

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String sql = "SELECT id, naziv, cena, besplatna_dostava FROM proizvod WHERE sifra = " + sifra;

			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			if (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				boolean besplatnaD = rset.getBoolean(index++);
				
				proizvod = new Proizvod(id, sifra, naziv, cena, besplatnaD);
			}
		} finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
			try {rset.close();} catch (Exception ex) {ex.printStackTrace();}
		}

		return proizvod;
	}
	
	public static List<Proizvod> getAll() throws Exception {
		List<Proizvod> proizvodi = new ArrayList<>();
//		Map

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String sql = "SELECT * FROM proizvod";
//			SELECT proizvod.* , porudzbina.proizvod  FROM proizvod LEFT JOIN  porudzbina ON porudzbina.proizvod = proizvod.id;

			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String sifra = rset.getString(index++);
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				boolean besplatnaD = rset.getBoolean(index++);
				
				Proizvod proizvod = new Proizvod(id, sifra, naziv, cena, besplatnaD);
				proizvodi.add(proizvod);
//				proizvod.getPorudzbina().add(rset.getInt(index++);)
			}
		} finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
		}

		return proizvodi;
	}
	
	public static List<String> getAllIzvestaj(LocalDateTime datumOd, LocalDateTime datumDo) throws Exception {
		List<String> izvestaj = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String sql = "SELECT proizvod.sifra, proizvod.naziv, SUM(CASE WHEN proizvod.besplatna_dostava = 0 THEN 1000 ELSE 0 END + proizvod.cena) AS 'prihod', MAX(porudzbina.datum) \r\n" + 
					"FROM porudzbine.proizvod LEFT JOIN porudzbina ON porudzbina.proizvod = proizvod.id \r\n" + 
					"WHERE porudzbina.datum >= '" + datumOd + "' AND porudzbina.datum <= '" + datumDo + "' GROUP BY proizvod.id ORDER BY prihod DESC";
			
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);

			while (rset.next()) {
				int index = 1;
				String sifra = rset.getString(index++);
				String naziv = rset.getString(index++);
				int sum = rset.getInt(index++);
				LocalDateTime datum = rset.getTimestamp(index++).toLocalDateTime();
				String line = sifra+","+naziv+","+sum+","+datum;
				izvestaj.add(line);
			}
		} finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
		}

		return izvestaj;
	}

}
