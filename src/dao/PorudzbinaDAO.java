package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import model.Porudzbina;
import model.Proizvod;

public class PorudzbinaDAO {
	
	public static Porudzbina getPorudzbinaByID(int id) throws Exception {
		Porudzbina porudzbina = null;

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String sql = "SELECT datum, ulica, broj, proizvod FROM porudzbina WHERE id = " + id;

			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			if (rset.next()) {
				int index = 1;
				LocalDateTime datum = rset.getTimestamp(index++).toLocalDateTime();
				String ulica = rset.getString(index++);
				int broj = rset.getInt(index++);
				Proizvod proizvod = ProizvodDAO.getProizvodByID(rset.getInt(index++));
				
				porudzbina = new Porudzbina(id, datum, ulica, broj, proizvod);
			}
		} finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
			try {rset.close();} catch (Exception ex) {ex.printStackTrace();}
		}

		return porudzbina;
	}
	
	public static List<Porudzbina> getAll() throws Exception {
		List<Porudzbina> porudzbine = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String sql = "SELECT * FROM porudzbina";

			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				LocalDateTime datum = rset.getTimestamp(index++).toLocalDateTime();
				String ulica = rset.getString(index++);
				int broj = rset.getInt(index++);
				Proizvod proizvod = ProizvodDAO.getProizvodByID(rset.getInt(index++));
				
				Porudzbina porudzbina = new Porudzbina(id, datum, ulica, broj, proizvod);
				porudzbine.add(porudzbina);
			}
		} finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
		}

		return porudzbine;
	}
	
	public static boolean add(Porudzbina porudzbina) throws Exception {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES (?, ?, ?, ?)";

			stmt = ConnectionManager.getConnection().prepareStatement(sql);
			int index = 1;
			stmt.setTimestamp(index++, Timestamp.valueOf(porudzbina.getDatum()));
			stmt.setString(index++, porudzbina.getUlica());
			stmt.setInt(index++, porudzbina.getBroj());
			stmt.setInt(index++, porudzbina.getProizvod().getId());

			return stmt.executeUpdate() == 1;
		} finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
		}
	}
	

}
