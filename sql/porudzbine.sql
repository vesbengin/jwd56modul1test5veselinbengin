DROP SCHEMA IF EXISTS porudzbine;
CREATE SCHEMA porudzbine DEFAULT CHARACTER SET utf8;
USE porudzbine;

CREATE TABLE proizvod (
	id INT NOT NULL AUTO_INCREMENT,
    sifra VARCHAR(50) UNIQUE NOT NULL,
    naziv VARCHAR(50) NOT NULL,
    cena DECIMAL(10,2) NOT NULL,
    besplatna_dostava BOOLEAN NOT NULL,
   
    PRIMARY KEY(id)
);

CREATE TABLE porudzbina (
	id INT NOT NULL AUTO_INCREMENT,
    datum DATETIME NOT NULL,
    ulica VARCHAR(50) NOT NULL,
    broj INT NOT NULL,
    proizvod INT NOT NULL,
   
    PRIMARY KEY(id),
    
    FOREIGN KEY (proizvod) REFERENCES proizvod(id)
	    ON DELETE RESTRICT
);

INSERT INTO proizvod (sifra, naziv, cena, besplatna_dostava) VALUES ('0001', 'Proizvod 1', 5000.00, false);
INSERT INTO proizvod (sifra, naziv, cena, besplatna_dostava) VALUES ('0002', 'Proizvod 2', 100000.00, false);
INSERT INTO proizvod (sifra, naziv, cena, besplatna_dostava) VALUES ('0003', 'Proizvod 3', 15000.00, true);

INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES ('2021-09-01 10:00', 'Bul. Oslobođenja', 10, 1);
INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES ('2021-09-01 14:00', 'Bul. Mihajla Pupina', 10, 1);
INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES ('2021-09-01 20:00', 'Bul. Oslobođenja', 20, 2);
INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES ('2021-09-02 10:30', 'Bul. Cara Lazara', 10, 1);
INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES ('2021-09-02 14:30', 'Bul. Oslobođenja', 30, 3);
INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES ('2021-09-03 10:00', 'Bul. Mihajla Pupina', 20, 2);
INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES ('2021-09-03 14:00', 'Bul. Mihajla Pupina', 30, 3);
INSERT INTO porudzbina (datum, ulica, broj, proizvod) VALUES ('2021-09-03 20:00', 'Bul. Cara Lazara', 20, 1);
